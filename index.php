<?php 
	require_once('Animal.php');
	require_once('Frog.php');
	require_once('Ape.php');

	$sheep = new Animal("shaun");
	echo "Animal Name : " . $sheep->name . "<br>"; // "shaun"
	echo "Legs : " . $sheep->legs . "<br>"; // 2
	echo "Cold Blood : " . $sheep->cold_blooded; // false

	echo "<br><br>";

	$kodok = new Frog("buduk");
	echo "Animal Name : " . $kodok->name , "<br>";
	echo "Legs : " . $kodok->legs . "<br>";
	echo $kodok->jump(); // "hop hop"

	echo "<br><br>";

	$sungokong = new Ape("kera sakti");
	echo "Animal Name : " . $sungokong->name , "<br>";
	echo "Legs : " . $sungokong->legs . "<br>"; // 2
	$sungokong->yell() // "Auooo"
 ?>